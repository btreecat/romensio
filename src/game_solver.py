'''
Created on Jan 30, 2012

@author: stanner
 This file is part of romensio.

    romensio is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    romensio is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with romensio.  If not, see <http://www.gnu.org/licenses/>.
'''
import numpy as np
import networkx as nx
import time


class RomensioSolver(object):
    '''Graph will use the a modified Coordinate class for nodes,
    and the edges will have a weight, which is the value of U + V
    '''
    game_board_graph = None
    n = 0
    game_engine = None
    move_counter = 0
    left_over = 0
    start_time = 0
    end_time = 0
    start = None


    def __init__(self, game_engine, starting_coordinate = None):
        '''
        Constructor
        '''
        self.game_engine = game_engine
        self.n = game_engine.get_n()
        self.game_board_graph = self.build_graph()

        if starting_coordinate != None:
            game_board_matrix = self.game_engine.get_board()
            self.start = Coordinate(starting_coordinate, game_board_matrix[starting_coordinate['y'], starting_coordinate['x']])


    def run_time(self):
        '''Get the time it took to solve the game in clock time'''
        return self.end_time - self.start_time

    def solve_game(self):
        '''
        Make a list nodes. 
        
        pick a node.
        look at its neighbors.
        does it have an edge with a weight of 10?
        if so remove the nodes
        
        pick a new node.
        check if node still in tree
        look at its neighbors does it have any edgese with a weight of 10?
        if not, add the edge/node with the smallest weight.
        Look at the neighbors of the two nodes.
        Are there any new edges that make the total path ten?
        if not, add the node with the smallest weight that does not cuase
        the path weight to bust
        
        if all neighbors would cause a bust, remove the last added node and pick
        the next lowset weight neighbor
        '''

        self.start_time = time.time()

        while True:
            found = False
            nodes = self.game_board_graph.nodes()
            #Favorite node
            if self.start != None:
                if self.start in nodes:
                    nodes.remove(self.start)
                    nodes.insert(0, self.start)
                else:
                    self.start = None

            for node in nodes:
                if self.game_board_graph.has_node(node):
                    '''For the given node, find a path of weight 10'''
                    path = self.find_path([node], [])
                    if path != False:
                        move = [self.to_position(c) for c in path]
                        valid = self.game_engine.submit_move(move)
                        if valid:
                            self.game_board_graph = self.build_graph()
                            self.move_counter = self.move_counter + 1
                            found = True
            if not found:
                break

        self.end_time = time.time()
        for spot in np.nditer(self.game_engine.get_board()):
            if spot != 0:
                self.left_over = self.left_over + 1
        return self

    def find_path(self, path, ignore = None):
        '''Takes a path of one or more nodes, and finds a neighbors until the
        weight of the path is 10.
        Returns a path of weight 10
            note : some times, it dosnt. Not sure why but some times it returns
                an under weight path. This is ok however as the game engine 
                rejects the move and the solver class does not rebuild the
                game_board_matrix and game_board_graph
                The only negative thing this does, is hurt running efficiency. 
        '''
        if len(path) == 0:
            return False
        path_weight = sum([p.value for p in path])

        neighbors = []
        for node in path:
            new_neighbors = self.game_board_graph.neighbors(node)

            for new in new_neighbors:
                if new not in neighbors and new not in path:
                    neighbors.append(new)

        if len(ignore) > 0:
            for node in ignore:
                if node in neighbors:
                    neighbors.remove(node)
        neighbors.sort()


        if len(neighbors) == 0:
            return False
        neighbor = neighbors[0]
        total_weight = path_weight + neighbor.value

        if total_weight == 10:
            path.append(neighbor)
            return path
        elif total_weight < 10:
            path.append(neighbor)
            return self.find_path(path, ignore)
        else:
            ignore.append(path.pop())
            return self.find_path(path, ignore)


    def to_position(self, coordinate):
        '''Simple method to be used with list comprehensions to transform
        a set of coordinates to a set of positions to be consumeable by
        other methods and the game_engine
        '''
        x = coordinate.x
        y = coordinate.y
        return {'x':x, 'y': y}


    def build_graph(self):
        '''For each node, build the edges and give them a weight of the 
        combination of the nodes
        '''

        '''
        Iterate over array, for each number in matrix, ceate a Coordinate.
        For the coordinate, check the neighborhood.
        For each neighbor, draw an edge from current itr Coordinate to its 
        neighbor. Add the value of the neighbor with the value of the itr to
        get the weight of the edge between itr and neighbor.
        repeat for remaining neighbors
        repeat for remaining itrs
        '''
        graph = nx.Graph()
        game_board_matrix = self.game_engine.get_board()
        it = np.nditer(game_board_matrix, flags = ['multi_index'])
        while not it.finished:

            value = it[0]
            if value > 0:   #checks the value of the matrix position before making a coordinate
                position = {'x':it.multi_index[1], 'y':it.multi_index[0]}
                coordinate = Coordinate(position, value)

                #Now, get list of neighbors for current coordinate
                neighbors = self.get_neighbors(position)
                for position in neighbors:
                    neighbor = Coordinate(position, game_board_matrix[position['y'], position['x']])
                    weight = coordinate.value + neighbor.value
                    if weight <= 10 and weight > coordinate.value:  #Makes cure the weight increases
                        graph.add_edge(coordinate, neighbor, weight = (weight))
            it.iternext()

        return graph

    def get_neighbors(self, position):
        '''
        Return a a list of coordinate positions that are valid neighbors
        '''

        x = position['x']
        y = position['y']

        neighbors = []

        neighborhood = [{'x':(x - 1), 'y':(y + 1)},
         {'x':x, 'y':(y + 1)},
         {'x':(x + 1), 'y':(y + 1)},
         {'x':(x - 1), 'y':y},
         {'x':(x + 1), 'y':y},
         {'x':(x - 1), 'y':(y - 1)},
         {'x':x, 'y':(y - 1)},
         {'x':(x + 1), 'y':(y - 1)}]

        for neighbor in neighborhood:
            if self.in_bounds(neighbor):
                neighbors.append(neighbor)

        return neighbors

    def in_bounds(self, position):
        '''Return true if position is in bounds'''
        if position['x'] < 0:
            return False
        elif position['y'] < 0:
            return False
        elif position['x'] >= self.n:
            return False
        elif position['y'] >= self.n:
            return False
        else:
            return True



class Coordinate(object):
    '''Internal class for handeling nodes'''

    x = 0
    y = 0
    value = 0

    def __init__(self, position, value = None):
        self.x = position['x']
        self.y = position['y']
        if value != None:
            self.value = int(value)

    def __str__(self):
        '''Make it pretty!'''
        return "(" + str(self.x) + "," + str(self.y) + ")"

    def __eq__(self, other):
        '''Override the == comparitor'''
        if other == None:
            return False
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False

    def __ne__(self, other):
        '''Override the != comparitor'''
        if other == None:
            return True
        if self.x == other.x and self.y == other.y:
            return False
        else:
            return True

    def __lt__(self, other):
        '''Override the < comparitor'''
        if other == None:
            return True
        if self.value > other.value:
            return False
        else:
            return True

    def __gt__(self, other):
        '''Override the > comparitor'''
        if other == None:
            return True
        if self.value > other.value:
            return True
        else:
            return False

    def __hash__ (self):
        '''If you override the eq and ne methods
            you must also override this function
            to use this Class in dictionaries.
        '''
        return hash(str(self))
