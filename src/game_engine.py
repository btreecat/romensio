'''
Created on Jan 23, 2012

@author: stanner
 This file is part of romensio.

    romensio is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    romensio is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with romensio.  If not, see <http://www.gnu.org/licenses/>.
'''


import numpy as np
import networkx as nx


class RomensioEngine(object):
    '''
    This class will be the actual game engine. You will be able to submit moves,
    request current board layout etc. It will adhear to the rules and only process
    valid moves.
    '''
    game_board = None
    n = 0

    def __init__(self, n = None, in_file = None):
        '''
        Constructor
        '''
        if n != None:
            self.n = n
            self.game_board = np.random.random_integers(1, 9, size = (self.n, self.n))
            print("Starting game board:")
            for row in self.game_board:
                r_string = str(row)
                r_string = r_string.replace('0', ' ')
                r_string = r_string.strip('[]')
                r_string = r_string.replace('\n', '')

                print(r_string)

            marker = self.n + (self.n - 1)
            print("=" * marker)
        else:
            with open(in_file, mode = 'r') as file:
                self.n = int(file.readline())
                self.game_board = np.loadtxt(file, dtype = int)


    def submit_move(self, position_list):
        '''
        This method will take a list of postions in the nxn matrix and sum them.
        If the sum === 10 then the tiles will have their values changed to 0. 
        
        It will return true if the move is executed successfully and false
        if the move fails or is invalid
        '''
        '''
        Postition list format  [[x,y], [x,y], [x,y], [x,y]...]
        '''
        if self.in_bounds(position_list) == False:
            return False

        if self.no_zero(position_list) == False:
            return False

        if self.sum_is_ten(position_list) == False:
            return False

        if self.are_touching(position_list) == False:
            return False

        self.remove_positions(position_list)
        return True

    def remove_positions(self, position_list):
        '''
        slide down all the numbers in the column for the positions being moved
        and fill the missing spaces at the top with 0's
        '''

        for position in position_list:

            x = position['x']
            y = position['y']

            i = 1
            if y - i >= 0:
                while i <= y:
                    value = self.game_board[y - i, x]
                    self.game_board[(y - i) + 1, x] = value
                    self.game_board[y - i, x] = 0
                    i = i + 1
            else:
                self.game_board[y, x] = 0



    def are_touching(self, position_list):
        '''
        Return true if all the positions are touching
        and false if they are disjoint
        '''
        coord_list = []
        for position in position_list:

            coord_list.append(Coordinate(position))

        graph = self.build_graph(position_list)

        return nx.is_connected(graph)

    def build_graph(self, position_list):
        '''
            This method builds and returns a graph of the coordinates 
            in the given list.
        '''
        return_graph = nx.Graph()

        for coordinate in position_list:
            x = coordinate['x']
            y = coordinate['y']

            node = Coordinate(coordinate)

            neighborhood = [Coordinate({'x':(x - 1), 'y':(y + 1)}),
                            Coordinate({'x':x, 'y':(y + 1)}),
                            Coordinate({'x':(x + 1), 'y':(y + 1)}),
                            Coordinate({'x':(x - 1), 'y':y}),
                            Coordinate({'x':(x + 1), 'y':y}),
                            Coordinate({'x':(x - 1), 'y':(y - 1)}),
                            Coordinate({'x':x, 'y':(y - 1)}),
                            Coordinate({'x':(x + 1), 'y':(y - 1)})]

            for direction in neighborhood:
                '''Now check each neighbor if it matches a coordinate in the list'''
                for neighbor in position_list:
                    n = Coordinate(neighbor)
                    if n == direction:

                        return_graph.add_edge(node, n)
                        break

        return return_graph


    def sum_is_ten(self, position_list):
        '''
        return true if sum is === 10
        false otherwise
        '''

        values = [self.game_board[position['y'], position['x']] for position in position_list]
        total = sum(values)

        if total == 10:
            return True
        else:
            return False

    def get_board(self):
        '''
        Get a copy of the game board as it is currently
        '''
        return self.game_board

    def get_n(self):
        '''Get a copy of the n value'''
        return self.n

    def in_bounds(self, position_list):
        '''Returns True if position is useable'''
        for position in position_list:
            if position['x'] < 0:
                return False
            elif position['y'] < 0:
                return False
            elif position['x'] >= self.n:
                return False
            elif position['y'] >= self.n:
                return False
            else:
                pass
        return True

    def no_zero(self, position_list):
        '''Returns false if position is useable'''
        for position in position_list:
            if self.game_board[position['y'], position['x']] == 0:
                return False
            else:
                pass
        return True


class Coordinate(object):
    '''
    This is an internal class that works as a node for building the graphs
    '''

    x = 0
    y = 0

    def __init__(self, position):
        self.x = position['x']
        self.y = position['y']


    def __str__(self):
        '''Make it pretty!'''
        return "(" + str(self.x) + "," + str(self.y) + ")"

    def __eq__(self, other):
        '''Override the == comparitor'''
        if other == None:
            return False
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False

    def __ne__(self, other):
        '''Override the != comparitor'''
        if other == None:
            return True
        if self.x == other.x and self.y == other.y:
            return False
        else:
            return True

    def __hash__ (self):
        '''If you override the eq and ne methods'''
        return hash(str(self))
