'''
Created on Jan 23, 2012

@author: stanner
 This file is part of romensio.

    romensio is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    romensio is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with romensio.  If not, see <http://www.gnu.org/licenses/>.
'''
import argparse
from game_engine import RomensioEngine
from game_solver import RomensioSolver
from multiprocessing import Pool
import random

def multi_solve(solver):
    return solver.solve_game()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Solution to Romensio game.')

    parser.add_argument('-n', '--dimension', metavar = 'N', type = int,
                   help = 'Integer for the N x N matrix size')
    parser.add_argument('-i', '--input', metavar = 'I',
                   help = 'File to use as input instead of random generation based on N')
    parser.add_argument('-p', '--processes', metavar = 'P', type = int,
                   help = 'How many subprocesses to spawn to make multiple solutions in parallel. The best one is then chosen')

    #read and verify the command line arguments
    args = parser.parse_args()
    if args.input == None and args.dimension == None:
        print("No integer N or input file I given. Try 'romensio.py -h' to access the help menu.")
    elif args.input == None:
        game = RomensioEngine(n = args.dimension)
    elif args.dimension == None:
        game = RomensioEngine(in_file = args.input)
    else:
        print("You can not supply both interger N and input file I. Try 'romensio.py -h' to access the help menu.")

    solver = None
    if args.processes != None:
        #Use multiprocess module to solve game in paralel and increase throughput
        start_positions = []
        solvers = []
        pool = Pool(args.processes)
        for i in range(args.processes):
            x = random.randint(0, (game.n - 1))
            y = random.randint(0, (game.n - 1))
            start_positions.append({'x': x, 'y': y})

        for position in start_positions:
            solvers.append(RomensioSolver(game, position))

        results = pool.map(multi_solve, solvers)
        sorted(results, key = lambda solver: solver.left_over)
        solver = results[-1]
    else:
        #Just the one please
        solver = RomensioSolver(game)
        solver.solve_game()

    game_board = solver.game_engine.get_board()
    for row in game_board:
        r_string = str(row)
        r_string = r_string.replace('0', ' ')
        r_string = r_string.strip('[]')
        r_string = r_string.replace('\n', '')
        print(r_string)
    print(solver.left_over)
    print(solver.move_counter)
    run_time = solver.run_time()
    if run_time >= 100:
        m = int(run_time / 60)
        s = run_time % 60
        print("Solver took: " + str(m) + ":" + str(s)[:4])
    else:
        print("Solver took: " + str(run_time)[:6] + 's')
    f = open("out.txt", "w", encoding = "utf-8")
    for row in game_board:
        r_string = str(row)
        r_string = r_string.replace('0', ' ')
        r_string = r_string.strip('[]')
        r_string = r_string.replace('\n', '')
        f.write(r_string + '\n')
    f.write(str(solver.left_over) + '\n')
    f.write(str(solver.move_counter) + '\n')
